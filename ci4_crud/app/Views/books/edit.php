<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Book Management System</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css'); ?>" >
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>" >
</head>
<body>

    
      <!-- \Config\Services::validation()->listErrors(); -->
    <div class="container-fluid bg-purple shadow-sm">
        <div class="conatiner pb-2 pt-2">
        <div class="text-white h4"> Book Management System</div>
    </div>
</div>
<div class="bg-white shadow-sm">
    <div class="container">
        <div class="row">
            <nav class="nav nav-underline">
                <div class="nav-link">Books /Edit</div>
            </nav>
        </div>
</div>
</div>
</div>
<div class="container mt-4">
        <div class="row">
    <div class="col-md-12 text-right">
     
        <a href="<?php echo base_url('/') ?>" class="btn btn-primary ">Back</a>
</div>
</div>
</div>
<div class="container mt-4">
<div class="row">
    <div class="col-md-12">
    <div class="card">
        <div class="card-header bg-purple text-white">
        <div class="card-header-title">Edit Books </div>
        </div>
        <div class="card-body" >
        <form name="createForm" id="createForm" method="post" action="<?php echo base_url('/edit/'.$book['id'])  ?>">
  <div class="form-group">
    <label for="Name">Name</label>
    <input type="text" name="name" value="<?php echo set_value('name',$book['title']); ?>" class="form-control" id="name"  placeholder="Enter Book Name">
<?php
//   session_start();
$validation = \Config\Services::validation();
// print_r($validation);
    if(isset($validation) && $validation->hasError('name')){
            echo $validation->getError('name');
    }
    
?>

</div>  
<div class="form-group">
    <label for="Author">Author Name</label>
    <input type="text" name="author" value="<?php echo set_value('author',$book['author']); ?>" class="form-control" id="Author"  placeholder="Enter Book Author">
    <?php
  
  $validation = \Config\Services::validation();
  // print_r($validation);
      if(isset($validation) && $validation->hasError('author')){
              echo $validation->getError('author');
      }
      
  ?>
    
</div>  
<div class="form-group">
    <label for="ISBN Number">ISBN Number</label>
    <input type="text" name="ISBN_Number" value="<?php echo set_value('author',$book['isbn_no']); ?>" class="form-control" id="ISBN_Number"  placeholder="Enter ISBN Number">
</div>  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
        </div>
    </div>
</div>
</div>
</div>

</body>
</html>