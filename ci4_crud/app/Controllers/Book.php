<?php
namespace App\Controllers;
use Config\Services;
use App\Models\BookModel;
class Book extends BaseController{
    protected $helpers = ['form'];
    
    public function index(){
        $session = \Config\Services::session();
        $data['session'] = $session;
        $model = new BookModel();
       $bookArray= $model->getRecords();
       $data['book'] = $bookArray;
       return view('books/list',$data);
    }

    public function create(){
        $session = \Config\Services::session();
        // $validation = \Config\Services::validation();
        helper('form');
        $data = [] ;
       
        if ( $this->request->getMethod() == 'post') {
            $input = $this->validate([

                'name' => [
                    'rules'  => 'required|min_length[3]',
                    'errors' => [
                        'required' => 'Name is required.'
                    ]
                ],
                'author' => [
                    'rules'  => 'required|min_length[3]',
                    'errors' => [
                        'required' => 'Author is required.'
                    ]
                ]
                    // 'name' => 'required | min_length[3]',
                    // 'author'=> 'required | min_length[3]',
            ]);
            if($input == true){
                    //form validate sucessfully

                    $model = new BookModel();
                    $model->save([
                            'title' =>$this->request->getPost('name'),
                            'author' =>$this->request->getPost('author'),
                            'isbn_no' =>$this->request->getPost('ISBN_Number'),

                        ]);
                        $session->setFlashdata('success','Record added sucessfully.');
                        return redirect()->to('/');
            }else{
                    // form not validate sucessfully

                $data['validation'] = $this->validator;
                
                        }
        }
        return view('books/create',   $data);
    }

    public function edit($id){
        $session = \Config\Services::session();
        // $validation = \Config\Services::validation();
        helper('form');
        $model = new BookModel();
        $book = $model->getRow($id);
        // print_r($book);
        if(empty($book)){
            $session->setFlashdata('error','Record not found.');
            return redirect()->to('/');
        }
        $data = [] ;
       $data['book'] = $book;
        if ( $this->request->getMethod() == 'post') {
            $input = $this->validate([

                'name' => [
                    'rules'  => 'required|min_length[3]',
                    'errors' => [
                        'required' => 'Name is required.'
                    ]
                ],
                'author' => [
                    'rules'  => 'required|min_length[3]',
                    'errors' => [
                        'required' => 'Author is required.'
                    ]
                ]
                    // 'name' => 'required | min_length[3]',
                    // 'author'=> 'required | min_length[3]',
            ]);
            if($input == true){
                    //form validate sucessfully

                    $model = new BookModel();
                    $model->update($id,[
                            'title' =>$this->request->getPost('name'),
                            'author' =>$this->request->getPost('author'),
                            'isbn_no' =>$this->request->getPost('ISBN_Number'),

                        ]);
                        $session->setFlashdata('success','Record updated sucessfully.');
                        return redirect()->to('/');
            }else{
                    // form not validate sucessfully

                $data['validation'] = $this->validator;
                
                        }
        }
        return view('books/edit',   $data);
    }

    public function delete($id){
        $session = \Config\Services::session();
        
        $model = new BookModel();
        $book = $model->getRow($id);
        if(empty($book)){
            $session->setFlashdata('error','Record not found.');
            return redirect()->to('/');
        }
        $model->delete($id);
        $session->setFlashdata('success','Record delete sucessfully.');
            return redirect()->to('/');

    }
}



?>