<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Book Management System</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css'); ?>" >
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>" >
</head>
<body>
    <div class="container-fluid bg-purple shadow-sm">
        <div class="conatiner pb-2 pt-2">
        <div class="text-white h4"> Book Management System</div>
    </div>
</div>
<div class="bg-white shadow-sm">
    <div class="container">
        <div class="row">
            <nav class="nav nav-underline">
                <div class="nav-link">Books /views</div>
            </nav>
        </div>
</div>
</div>
</div>
<div class="container mt-4">
        <div class="row">
    <div class="col-md-12 text-right">
     
        <a href="<?php echo base_url('/create')?>" class="btn btn-primary ">Add</a>
</div>
</div>
</div>
<div class="container mt-4">
<div class="row">
<div class="col-md-12">
    <?php
      if(!empty($session->getFlashdata('success'))){
        ?>

        <div class="alert alert-success">
            <?php echo $session->getFlashdata('success'); ?>
      </div>
      <?php
      }
    ?>
    <?php
      if(!empty($session->getFlashdata('error'))){
        ?>

        <div class="alert alert-danger">
            <?php echo $session->getFlashdata('error'); ?>
      </div>
      <?php
      }
    ?>
</div>
    <div class="col-md-12">
    <div class="card">
        <div class="card-header bg-purple text-white">
        <div class="card-header-title">Books </div>
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <tr>
        <th>ID</th>
        <th>TITLE</th>
        <th>ISBN NO</th>
        <th>AUTHOR</th>
        <th>ACTION</th>
</tr>

<?php if(!empty($book)) {
    
    foreach($book as $b){
    ?>

        <tr>
        <td><?php echo $b['id'] ?></td>
        <td><?php echo $b['title'] ?></td>
        <td><?php echo $b['isbn_no'] ?></td>
        <td><?php echo $b['author'] ?></td>
        <td>
            <a href="<?php echo base_url('/edit/' .$b['id']);?>" class="btn btn-primary btn-sm">Edit</a>
            <a href="#" onClick="deleteConfirm(<?php echo $b['id'] ?>);" class="btn btn-danger btn-sm">Delete</a>
        </td>
</tr>
<?php }
} 
else {
    ?>

    <tr >
        <td colspan="5" >Records not found</td>
</tr>
<?php } ?>

        </table>
        </div>
    </div>
</div>
</div>
</div>

</body>
</html>
<script>
    function deleteConfirm(id){
        // alert(id);
        if(confirm("Are you sure want to delete?")){
            window.location.href='<?php echo base_url('/delete/') ?>/'+id;
        }
    }
</script>