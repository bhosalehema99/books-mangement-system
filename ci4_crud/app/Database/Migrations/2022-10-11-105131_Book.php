<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Book extends Migration
{
    public function up()
    {
        //
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'title' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
            ],
            'isbn_no' => [
                'type' => 'INT',
                'constraint' => '100',
            ],
            'author' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
            ],
            'created_at datetime default current_timestamp',
            // 'created_at' => [
            //     'type' => 'DATETIME',
            //     // 'default' => 'CURRENT_TIME',
            //     'constraint' => '100',
            // ],
           
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('book',true);
}
    

    public function down()
    {
        //
        $this->forge->dropTable('book');
    }
}
